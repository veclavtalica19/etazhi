import ../types/obj, tiles

## Objects are defined in terms of their apparent qualities, but not composition

template `&`(a: Object): Variant =
  Variant(kind: vkAddr, addr_v: unsafeaddr a)

const outside* = defineObject:
  texture = `tileOutside`
  outside

const wall* = defineObject:
  texture = `tileWall`
  size = huge
  colossal

const heart* = defineObject:
  texture = `tileHeart`
  size = small
  color = red
  fleshy

const blood* = defineObject:
  texture = `tileBlood`
  color = red
  liquid # Special category of objects

const humanoid* = defineObject:
  texture = `tileHumanoid`
  size = big
  # todo: Solve the error in nim's compiler caused from this
  # filling = &blood
  # contents = [&heart]
  animated
  fleshy

const knife* = defineObject:
  texture = `tileKnife`
  size = small
  sharp # Means that it could cut things and poke holes to expose the `filling`
  metallic

const pan* = defineObject:
  texture = `tilePan`
  size = medium
  metallic
  vessel # Means that it could hold liquids

# todo: Fridge
# const xxx* = defineObject:
#   texture = `xxx`
#   size = big
#   # `openable` make it so moving on this object will cause dropping of its `contents`
#   openable

# const kitchenEnv* = defineEnv:
#   knife
#   pan
