import std/macros
import pkg/nimraylib_now
import ../types/vec

type
  TexturePageId = uint8
  TileId* = uint64
  TexturePage* = object
    path*: string
    # todo: Populate at comptime?
    dims*: Vec
    tile_size*: Vec
     # todo: We can infer that from tile size and checking the file at comptime
    tiles_per_row*: uint32
    tiles*: seq[Tile]
    txt*: Texture
  Tile* = object
    ## Tile within TexturePage
    ofs*: Vec

var id {.compiletime.} = 0.TexturePageId
var texture_vector {.compiletime.}: seq[TexturePage]
var runtime_texture_vector: seq[TexturePage]

proc initTextures* = once:
  for t in runtime_texture_vector.mitems:
    echo "Loading texture from: ", t.path
    let img = loadImage t.path.cstring
    doAssert img.data != nil
    t.txt = loadTextureFromImage img
    # todo: Assert whether texture loaded successfully
    img.unloadImage

func page*(id: TileId): TexturePageId {.inline.} =
  const bts = sizeof(uint64) - sizeof(uint8)
  TexturePageId id shr bts.TileId

func tile*(id: TileId): int {.inline.} =
  const bts = sizeof(uint64) - sizeof(uint8)
  int id and TexturePageId.high shl bts

proc getPage*(id: TileId): TexturePage {.inline.} =
  runtime_texture_vector[id.page]

proc defineTexturePage(path: string, tile_size: Vec, tiles_per_row: uint32): TexturePageId {.compiletime.} =
  texture_vector.add TexturePage(
    path: path,
    tile_size: tile_size,
    tiles_per_row: tiles_per_row)
  result = id
  id.inc

proc defineTile(p: TexturePageId, y, x: uint32): TileId {.compiletime.} =
  # todo: Assert whether x, y is valid within page
  const bts = sizeof(uint64) - sizeof(uint8)
  doAssert texture_vector.len > p.int
  doAssert texture_vector[p].tiles.len < (1 shl bts)
  let (tile_width, tile_height) = texture_vector[p].tile_size
  texture_vector[p].tiles.add:
    Tile(ofs: (x * tile_width, y * tile_height))
  texture_vector[p].tiles.high.TileId or (p shl bts).TileId

const
  page0 = defineTexturePage("assets/graphics/page0.gif", (24u32, 24u32), 10u32)

  tileHumanoid* = page0.defineTile(0, 0)
  tileHeart*    = page0.defineTile(0, 1)
  tileBlood*    = page0.defineTile(0, 2)
  tileSolid*    = page0.defineTile(1, 0)
  tileWall*     = page0.defineTile(1, 1)
  tileError*    = page0.defineTile(1, 2)
  tileOutside*  = page0.defineTile(1, 2)
  tileKnife*    = page0.defineTile(2, 0)
  tilePan*      = page0.defineTile(2, 1)

runtime_texture_vector = static: texture_vector
