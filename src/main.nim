# Strata
import nimraylib_now
import ./content/[objects, tiles]
import ./types/world as world_type
import ./render

const window_size = (640u32, 480u32)

var world = initWorld (32u32, 32u32)
var camera = (0u32, 0u32)

world.emplace (4u32, 4u32), humanoid

proc render =
  beginDrawing:
    clearBackground DarkGray
    world.render camera.viewport(window_size, world.dims)

proc loop =
  render()

proc main =
  initWindow window_size.width.cint, window_size.height.cint, "Total state of disrepair"
  initTextures()
  while not windowShouldClose():
    loop()
  closeWindow()

when isMainModule:
  main()
