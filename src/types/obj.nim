import std/[macros, strformat, sequtils]
import ./variant
export variant

# todo: Ensure that object properties have appropriate variant value on comptime
#       This will also allow optimization based on coercion to appropriate values AOT

type
  Object* = object
    # todo: Make it possible to store props in array for static tempate objects
    props*: seq[Property]
  Property* = tuple
    key: Key
    val: Variant
  Key* = uint64

func `$`*(p: Property): string =
  result = "("
  for i in 0..7:
    let ch = (p.key and (0xff shl (i * 8)).Key) shr (i * 8)
    if ch == 0: break
    result &= ch.char
  result &= ": " & $p.val & ")"  

converter toKey*(s: string): Key =
  doAssert s.len <= sizeof Key, fmt"Literal is too long for being a key: {s}"
  for i, ch in s:
    result = result or (ch.uint64 shl (i * 8))

func toKey(exp: NimNode): NimNode =
  doAssert exp.kind == nnkIdent, fmt"Expected identifier, found: {exp.repr}"
  let key = exp.strVal.toKey
  return quote do: `key`.Key

func toVariant(exp: NimNode): NimNode =
  let v = if exp.kind == nnkIdent: exp.toKey else: exp
  return quote do: `v`.toVariant

macro defineObject*(definition: untyped): untyped = 
  let body = newStmtList()
  let ident = nskVar.genSym
  body.add quote do:
    var `ident` = Object()
  for def in definition:
    var key, val: NimNode
    case def.kind:
    of nnkIdent:
      key = def.toKey
      val = quote do: Variant.empty
    of nnkAsgn:
      doAssert def.len == 2
      doAssert def[0].kind == nnkIdent, fmt"Expected identifier: {def.repr}"
      key = def[0].toKey
      val = def[1].toVariant
    of nnkCall:
      doAssert def.len == 2
      doAssert def[0].kind == nnkIdent, fmt"Expected identifier: {def.repr}"
      doAssert def[1].kind == nnkStmtList, fmt"Expected statement list: {def.repr}"
      let list = newNimNode(nnkBracket).add:
        def[1].mapIt(it.toVariant)
      key = def[0].toKey
      val = list.toVariant
    else:
      error fmt"Unexpected syntax in object definition: {def.repr}"
    body.add quote do:
      `ident`.props.add (`key`, `val`)
  body.add quote do: `ident`
  result = body.newBlockStmt

func find*(o: Object, k: Key): int =
  ## Returns index of property if it exists, -1 otherwise
  for i, (key, _) in o.props:
    if key == k:
      return i
  return -1

template withItOr*(o: Object, k, y, n: untyped): untyped =
  if (let id = o.find k; id != -1):
    let it {.inject.} = o.props[id].val
    y
  else:
    n
