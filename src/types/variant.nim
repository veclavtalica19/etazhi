import std/[sequtils]

# todo: What about nanboxing?

type
  VariantKind* = enum
    vkNone
    vkInt
    vkUint
    vkFloat
    vkSeq
    vkPoolStr
    vkAddr

  Variant* = object
    case kind*: VariantKind:
    of vkNone: discard
    of vkInt: int_v*: int64
    of vkUint: uint_v*: uint64
    of vkFloat: float_v*: float64
    of vkSeq: seq_v*: seq[Variant]
    of vkPoolStr: pool_str_v*: int ## Point to entry in `string_pool`
    of vkAddr: addr_v*: pointer

# todo: Prettier printing

{.push inline.}

converter toVariant*(a: SomeInteger): Variant =
  when a is SomeSignedInt:
    Variant(kind: vkInt, int_v: a)
  else:
    Variant(kind: vkUint, uint_v: a)

converter toVariant*(a: pointer): Variant =
  Variant(kind: vkAddr, addr_v: a)

converter toVariant*(a: openArray[Variant]): Variant =
  Variant(kind: vkSeq, seq_v: a.toSeq)

func toVariant*(a: Variant): Variant = a

func empty*(t: typedesc[Variant]): Variant =
  Variant(kind: vkNone)

{.pop.}
