type
  Rect* = tuple
    # todo: Use Vec type instead, defined as another tuple
    x: uint32
    y: uint32
    width: uint32
    height: uint32

iterator items*(r: Rect): tuple[x: uint32, y: uint32] {.inline.} =
  for x in r.x..<r.x + r.width:
    for y in r.y..<r.y + r.height:
      yield (x, y)
