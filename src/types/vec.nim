
type
  Vec* = tuple
    x: uint32
    y: uint32

{.push inline.}

func width*(v: Vec): uint32 = v.x
func height*(v: Vec): uint32 = v.y
func `=width`*(v: var Vec, val: uint32) = v.x = val
func `=height`*(v: var Vec, val: uint32) = v.y = val

func area*(v: Vec): uint32 = v.x * v.y
func `div`*(a, b: Vec): Vec = (a.x div b.x, a.y div b.y)
func `div`*(a: Vec, s: uint32): Vec = (a.x div s, a.y div s)
func `in`*(a, b: Vec): bool = a.x <= b.x and a.y <= b.y

{.pop.}
