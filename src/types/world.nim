from std/sequtils import repeat
import obj, vec
export vec

type
  World* = object
    matrix*: seq[Object]
    dims*: Vec

func initWorld*(dims: Vec): World =
  World(matrix: Object.default.repeat(dims.area), dims: dims)

func emplace*(w: var World, at: Vec, o: sink Object) =
  assert at in w.dims
  # todo: Check whether its free to emplace?
  w.matrix[at.x + at.y * w.dims.x] = o
