import pkg/nimraylib_now
import ./types/[world, obj, rect]
import ./content/tiles

# todo: Shouldnt be hardcoded, probably
const grid_offset_x = 24
const grid_offset_y = 24

func viewport*(p, dims, lim: Vec): Rect =
  let size = dims div (uint32 grid_offset_x, uint32 grid_offset_y)
  let offset = size div 2
  (max(0, p.x.int64 - offset.x.int64).uint32 - max(0, (p.x + offset.x).int64 - lim.x.int64).uint32,
    max(0, p.y.int64 - offset.y.int64).uint32 - max(0, (p.y + offset.y).int64 - lim.y.int64).uint32,
    size.x, size.y)

func toColor*(c: Key): Color =
  if c == "red": Red
  elif c == "blue": Blue
  else: White

proc render*(o: Object, x, y: float32) =
  if (let id = o.find("texture"); id != -1):
    let color = o.withItOr("color", it.uint_v.toColor, White)
    let page_id = o.props[id].val.uint_v
    let page = page_id.getPage
    let tile = page.tiles[page_id.tile]
    page.txt.drawTextureRec(
      Rectangle(
        x: float32 tile.ofs.width,
        y: float32 tile.ofs.height,
        width: float32 page.tile_size.width,
        height: float32 page.tile_size.height),
      Vector2(x: x, y: y),
      color)

proc render*(w: World, r: Rect) =
  for (x, y) in r:
    w.matrix[x + y * w.dims.width].render(
      float32 (x - r.x) * grid_offset_x,
      float32 (y - r.y) * grid_offset_y)
